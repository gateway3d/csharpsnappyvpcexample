﻿using System;

namespace ConsoleApp
{   
    class MainClass
    {       
        private const string ClientId = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
        private const string ClientSecret = "";

        public static void Main(string[] args)
        {
            var api = new SnappyVpc.GraphQl.Api(ClientId, ClientSecret);

            Action checkDone = () =>
            {
                if (api.AllCompleted)
                {
                    Console.WriteLine("All done");

                    Environment.Exit(0);
                }
            };

            api.CompletionEvent += (sender, e) =>
            {
                // At this point the image for the specific product is now available.
                //
                // Recommend writing PrintJob.ref and snapshot URL to
                // your database somewhere (you will need PrintJob.ref to create
                // the order later on anyway).
                //
                // The presence of said database record could be a trigger to your website
                // frontend that the thumbnail and product are now ready.

                Console.WriteLine($"Completion for product {e.BaseProduct.Id}, got print job ref {e.PrintJob.Ref}: {e.PrintJob.Snapshots.Medium}");

                checkDone();
            };

            api.ErrorEvent += (sender, e) =>
            {
                Console.WriteLine($"Error for product {e.BaseProduct.Id}: {e.Message}");

                checkDone();
            };

            // this represents the user's text
            string text = "hello, world";

            // this represents a user uploaded image
            SnappyVpc.GraphQl.Types.ImageInput image = new SnappyVpc.GraphQl.Types.ImageInput()
            {
                // "original" is used when generating the final artwork after 
                // an order has been created
                Original = "https://oms.custom-gateway.net/v2/images/retail-integrations/csv-gateway3d.png",

                // "preview" should be a scaled down version of the user's image (i.e. max 800x800
                // that is used when generating the product preview
                //
                // whilst preview could technically be the same as original, it is
                // recommended that you provide a scaled down version for performance reasons.
                //
                // i.e. if the product thumbnail generator has to load a 10MB high res
                // user image then it will be slow.

                Preview = "https://oms.custom-gateway.net/v2/images/retail-integrations/csv-gateway3d.png"

                // NOTE: Both URLs need to be publicly available and need to be
                // available for long enough for our artwork generation system
                // to process the final order.
                //
                // if re-ordering is required then the URLs need to be permanently available
                // as a reorder will trigger a new artwork generation
            };

            int categoryId = 40109;

            api.Generate(text, image, categoryId);

            Console.ReadKey();
        }
    }
}
