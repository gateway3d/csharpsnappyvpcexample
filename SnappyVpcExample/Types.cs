﻿namespace SnappyVpc.GraphQl.Types
{
    public class ImageInput
    {
        public string Original;
        public string Preview;
    }

    public enum AsyncTaskStatus
    {
        Pending = 0,
        InProgress = 0x20,
        Completed = 0x40,
        SysError = 0xFE,
        Error = 0xFF
    }

    public class Snapshots
    {
        public string Large;
        public string Medium;
        public string Small = null;

        public static Snapshots FromJson(System.Json.JsonValue jsonValue)
        {
            return new Snapshots
            {
                Large = (string)jsonValue["large"],
                Medium = (string)jsonValue["medium"],
                Small = (string)jsonValue["small"]
            };
        }
    }

    public class PrintJob
    {
        public string Ref;

        public Snapshots Snapshots;
        public Product Product;

        public static PrintJob FromJson(System.Json.JsonValue jsonValue)
        {
            return new PrintJob
            {
                Ref = (string)jsonValue["ref"],

                Snapshots = jsonValue.ContainsKey("snapshots") ? Snapshots.FromJson(jsonValue["snapshots"]) : null,
                Product = jsonValue.ContainsKey("product") ? Product.FromJson(jsonValue["product"]) : null
            };
        }
    }

    public class AsyncTask
    {
        public System.Guid Uuid;
        public string Context;
        public AsyncTaskStatus Status;

        public PrintJob PrintJob;

        public static AsyncTask FromJson(System.Json.JsonValue jsonValue)
        {
            return new AsyncTask
            {
                Context = (string)jsonValue["context"],
                Status = AsyncTask.StatusFromGraphQlEnum(jsonValue["status"]),
                Uuid = System.Guid.Parse(jsonValue["uuid"]),

                PrintJob = jsonValue.ContainsKey("print_job") ? PrintJob.FromJson(jsonValue["print_job"]) : null
            };
        }

        private static AsyncTaskStatus StatusFromGraphQlEnum(string status)
        {
            switch (status)
            {
                case "PENDING":
                    return Types.AsyncTaskStatus.Pending;
                case "COMPLETED":
                    return Types.AsyncTaskStatus.Completed;
                case "SYS_ERROR":
                    return Types.AsyncTaskStatus.SysError;
                case "ERROR":
                    return Types.AsyncTaskStatus.Error;
                case "IN_PROGRESS":
                    return Types.AsyncTaskStatus.InProgress;
                default:
                    throw new System.Exception($"Invalid status {status}");
            }
        }
    }

    public class Product
    {
        public int? Id;
        public string SupplierSku;
        public string RetailSku;
        public string Name;

        public static Product FromJson(System.Json.JsonValue jsonValue)
        {
            return new Product
            {
                Id = jsonValue.ContainsKey("id") ? (int)jsonValue["id"] : new System.Nullable<int>(),

                SupplierSku = jsonValue.ContainsKey("productCode") ? (string)jsonValue["productCode"] : null,

                RetailSku = jsonValue.ContainsKey("retail_sku") ? (string)jsonValue["retail_sku"] : null,

                Name = jsonValue.ContainsKey("name") ? (string)jsonValue["name"] : null
            };
        }
    }
}