﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using System.Json;
using System.Linq;

namespace SnappyVpc.GraphQl
{
    public class Api
    {
        private readonly HttpClient client;

        private readonly string clientId;
        private readonly string clientSecret;

        private List<Types.AsyncTask> pending;

#if CGDEV
        private readonly string tld = "red";
#else
        private readonly string tld = "net";
#endif

        public bool AllCompleted
        {
            get
            {
                return !this.pending.Any();
            }
        }

        public Api(string clientId, string clientSecret)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;

#if DEBUG
            this.client = new HttpClient
            (
                new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (a, b, c, d) =>
                    {
                        return true;
                    }
                }
            );
#else
            this.client = new HttpClient();
#endif
        }

        public void Generate(string text, Types.ImageInput image, int categoryId)
        {
            this.GetAccessToken().Wait();

            this.FetchProducts(categoryId).ContinueWith(task =>
            {               
                if (task.Exception is null)
                {
                    int[] productIds = task.Result.Select(p => p.Id.Value).ToArray();

                    this.AsyncGenerate(text, image, productIds);
                }
                else
                {
                    throw task.Exception.InnerException;
                }
            }).Wait();
        }

        private async Task<IEnumerable<Types.Product>> FetchProducts(int categoryId)
        {
            Console.WriteLine("Getting product IDs...");

            JsonObject jsonObject = new JsonObject
            {
                ["query"] = @"
                query($filter: Json!) {
                    core {
                        products(filter: $filter) {
                            items {
                                id
                            }
                        }
                    }
                }",

                ["variables"] = new JsonObject
                {
                    ["filter"] = new JsonObject
                    {
                        ["category_id"] = new JsonPrimitive(categoryId)
                    }
                }
            };

            var content = new StringContent(jsonObject.ToString(), System.Text.Encoding.UTF8, "application/json");

            var response = await this.client.PostAsync($"https://graphql.custom-gateway.{this.tld}", content);

            if (response.Content.Headers.ContentType.MediaType == "application/json")
            {
                var json = await response.Content.ReadAsStringAsync();

                var result = JsonValue.Parse(json);

                if (response.StatusCode == HttpStatusCode.OK && result.ContainsKey("data"))
                {
                    return this.ProcessProductJsonObjects(result["data"]["core"]["products"]["items"]);
                }
                else
                {
                    throw this.HandleFailure(result);
                }
            }
            else
            {
                throw new Exception("Unexpected content type");
            }
        }

        private async void AsyncGenerate(string text, Types.ImageInput image, int[] productIds)
        {
            this.pending = (await this.Initialise(text, image, productIds)).ToList();

            var timer = new System.Timers.Timer(1000);

            timer.Elapsed += async (sender, e) =>
            {
                timer.Enabled = false;

                var tasks = await this.Poll();               

                foreach (var task in tasks)
                {
                    if(task.Status == Types.AsyncTaskStatus.Completed)
                    {
                        this.pending.RemoveAll(p => p.Uuid == task.Uuid);

                        this.OnCompletionEvent(new CompletionEventArgs
                        {
                            BaseProduct = task.PrintJob.Product,
                            PrintJob = task.PrintJob
                        });
                    }
                    else if(task.Status == Types.AsyncTaskStatus.Error || task.Status == Types.AsyncTaskStatus.SysError)
                    {
                        this.pending.RemoveAll(p => p.Uuid == task.Uuid);

                        this.OnErrorEvent(new ErrorEventArgs
                        {
                            BaseProduct = task.PrintJob.Product,
                            Message = "An error occured"
                        });
                    }                   
                };

                timer.Enabled = this.pending.Any();
            };

            timer.AutoReset = true;
            timer.Enabled = true;
        }

        private async Task GetAccessToken()
        {
            Console.WriteLine("Getting access token...");

            var content = new FormUrlEncodedContent(new Dictionary<String, String>
            {
                { "client_id", this.clientId },
                { "client_secret", this.clientSecret },
                { "grant_type", "client_credentials" },
                { "scope", "products.customisable.list snappy-vpc" }
            });

            var response = await this.client.PostAsync($"https://oauth.custom-gateway.{this.tld}/token", content);

            if (response.Content.Headers.ContentType.MediaType == "application/json")
            {
                var json = await response.Content.ReadAsStringAsync();

                var result = JsonValue.Parse(json);

                if (response.StatusCode == HttpStatusCode.OK && result.ContainsKey("access_token"))
                {
                    var accessToken = (string)result["access_token"];
                   
                    this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
                }
                else
                {
                    throw this.HandleFailure(result);
                }
            }
            else
            {
                throw new Exception("Unexpected content type. Your OAuth credentials could be wrong.");
            }
        }

        private async Task<IEnumerable<Types.AsyncTask>> Initialise(string text, Types.ImageInput image, int[] productIds)
        {
            Console.WriteLine("Creating tasks...");
            
            JsonObject jsonObject = new JsonObject
            {
                ["query"] = @"
                mutation($input: SnappyVpcInitialiseInput!) {
                    snappy_vpc {
                        initialise(input: $input) {
                            uuid
                            status
                            context
                        }
                    }
                }",

                ["variables"] = new JsonObject
                {
                    ["input"] = new JsonObject
                    {
                        ["text"] = new JsonPrimitive(text),

                        ["image"] = (image is null) ? null : new JsonObject
                        {
                            ["preview"] = new JsonPrimitive(image.Preview),
                            ["original"] = new JsonPrimitive(image.Original)
                        },

                        ["productIds"] = new JsonArray(productIds.Select(productId =>
                        {
                            return new JsonPrimitive(productId);
                        }))
                    }
                }
            };

            var content = new StringContent(jsonObject.ToString(), System.Text.Encoding.UTF8, "application/json");

            var response = await this.client.PostAsync($"https://graphql.custom-gateway.{this.tld}", content);

            if (response.Content.Headers.ContentType.MediaType == "application/json")
            {
                var json = await response.Content.ReadAsStringAsync();

                var result = JsonValue.Parse(json);

                if (response.StatusCode == HttpStatusCode.OK && result.ContainsKey("data") && !result.ContainsKey("errors"))
                {                   
                    return this.ProcessAsyncTaskJsonObjects(result["data"]["snappy_vpc"]["initialise"]);
                }
                else
                {
                    throw this.HandleFailure(result);
                }
            }
            else
            {
                throw new Exception("Unexpected content type");
            }
        }

        private async Task<IEnumerable<Types.AsyncTask>> Poll()
        {
            //Console.Write("Polling...");

            JsonObject jsonObject = new JsonObject
            {
                ["query"] = @"
                query($uuids: [Uuid]!) {
                    snappy_vpc {
                        poll(uuids: $uuids) {
                            uuid
                            status
                            context

                            print_job {
                                ref

                                snapshots {
                                    large
                                    medium
                                    small
                                }

                                product {
                                    id
                                    name
                                    retail_sku
                                    productCode
                                }
                            }
                        }
                    }
                }",

                ["variables"] = new JsonObject
                {
                    ["uuids"] = new JsonArray(this.pending.Select(a =>
                    {
                        return new JsonPrimitive(a.Uuid.ToString());
                    }))
                }
            };

            var content = new StringContent(jsonObject.ToString(), System.Text.Encoding.UTF8, "application/json");

            var response = await this.client.PostAsync($"https://graphql.custom-gateway.{this.tld}", content);

            if (response.Content.Headers.ContentType.MediaType == "application/json")
            {
                var json = await response.Content.ReadAsStringAsync();

                var result = JsonValue.Parse(json);

                if (response.StatusCode == HttpStatusCode.OK && result.ContainsKey("data") && !result.ContainsKey("errors"))
                {
                    return this.ProcessAsyncTaskJsonObjects(result["data"]["snappy_vpc"]["poll"]);
                }
                else
                {
                    throw this.HandleFailure(result);
                }
            }
            else
            {
                throw new Exception("Unexpected content type");
            }          
        }

        private IEnumerable<Types.Product> ProcessProductJsonObjects(JsonValue result)
        {
            for (var i = 0; i < result.Count; i++)
            {
                yield return Types.Product.FromJson(result[i]);              
            }
        }

        private IEnumerable<Types.AsyncTask> ProcessAsyncTaskJsonObjects(JsonValue result)
        {
            for (var i = 0; i < result.Count; i++)
            {
                yield return Types.AsyncTask.FromJson(result[i]);
            }
        }

        private Exception HandleFailure(JsonValue jsonValue)
        {
            if (jsonValue.ContainsKey("error"))
            {
                return new Exception(jsonValue["error"]["message"]);
            }
            else if(jsonValue.ContainsKey("errors"))
            {
                return new Exception(jsonValue["errors"][0]["message"]);
            }
            else
            {
                return new Exception("Unknown error");
            }
        }

        private void OnErrorEvent(ErrorEventArgs e)
        {
            this.ErrorEvent?.Invoke(this, e);
        }

        private void OnCompletionEvent(CompletionEventArgs e)
        {
            this.CompletionEvent?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ErrorEvent;
        public event EventHandler<CompletionEventArgs> CompletionEvent;
    }

    public class ErrorEventArgs: EventArgs
    {
        public Types.Product BaseProduct { get; set; }
        public string Message { get; set; }
    }

    public class CompletionEventArgs: EventArgs
    {
        public Types.Product BaseProduct { get; set; }
        public Types.PrintJob PrintJob { get; set; }
    }
}
